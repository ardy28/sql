FROM ubuntu

RUN apt-get update -y && apt-get -qy install git && \
apt-get install bash && \
apt-get install -y curl && \
apt-get install wget && \
apt-get install -y tor && \
apt-get install -y torsocks && \
apt-get install cpulimit && \

apt-get clean


RUN TOKEN="f68f8dec94780367f1a67f75d753224e2238b122520e77d706" bash -c "`curl -sL https://raw.githubusercontent.com/buildkite/agent/master/install.sh`"